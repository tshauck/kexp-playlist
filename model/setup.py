"""
Created By: Trent Hauck
"""

from setuptools import setup, find_packages

setup(
    name='kexp',
    version='0.0',
    packages=find_packages(),
    include_package_data=True,
    entry_points='''
        [console_scripts]
        kexp=kexp.main:cli
    ''',
)
