""" model.py

Functions to work with the model.

"""

from keras.models import Sequential
from keras.layers import Dense, LSTM, Activation, Dropout, Embedding
from keras.optimizers import RMSprop


def build_model(max_features, input_length):
    """Build Model.

    Build the Keras model.

    Args:
        max_features: The number of input features, this is input songs
            to consider.
        input_length: How long of sequence length to consider.

    Returns:
        model: Return the model to be refit.
    """

    model = Sequential()
    model.add(Embedding(max_features, 128, input_length=input_length, dropout=0.2))
    model.add(LSTM(128, dropout_W=0.2, dropout_U=0.2, return_sequences=True))
    model.add(LSTM(300, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(500, return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(max_features))
    model.add(Activation('softmax'))

    optimizer = RMSprop(lr=0.0001, clipnorm=0.5, clipvalue=0.5)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    return model
