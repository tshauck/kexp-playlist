"""
Created By: Trent Hauck
Date: 2016-08-20T16:24:59
License: Private use of Trent Hauck
"""

from kexp.model_endpoint import Model
import kexp.gcs as g
import kexp.model as m
import kexp.data as d
import kexp.repository as r
from kexp.log import L

import falcon

import os
import logging

L.setLevel(logging.INFO)

bucket = os.getenv("BUCKET")
assert bucket is not None, "$BUCKET env is not set."

repo = r.GcsRepository(bucket)

model, tag = repo.load_model()
L.info("using tag %s", tag)

engine = d.create_sql_engine()
songs_to_index, index_to_songs = d.get_indexes(engine, tag)

application = falcon.API()

model_api = Model(model, L, engine, songs_to_index, index_to_songs)

application.add_route(model_api.ROUTE, model_api)
L.info("starting server")
