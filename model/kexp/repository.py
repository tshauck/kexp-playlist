"""
Created By: Trent Hauck
Date: 2016-09-05T10:06:47
License: Private use of Trent Hauck
"""

import json
import glob
import uuid
from keras import models

from gcloud import storage

from kexp.log import L
import kexp.transformations as t


class Repository(object):
    """A base class for a repository. """

    def __init__(self):
        pass

    def get_collections(self, select_func=None):
        """Get collections.

        Returns the list of collections which were
        downloaded from the whatever source.

        """

        raise NotImplementedError("Subclass must implement.")


class FileRepository(Repository):
    """A file repository."""

    def __init__(self, pattern):
        self.pattern = pattern

    def _yield_files(self):
        """ Yield Files. """

        fs = glob.glob(self.pattern)

        for f in fs:
            L.debug("yield file %s", f)
            yield f

    def yield_collections(self,
                          n_collections=None,
                          select_func=lambda x: True):
        """Get collections.

        Args:
            select_func: A function that returns true if the
                function should be selected.
            n_collections: How many songs to yield.

        """

        i = 0
        for f in self._yield_files():
            with open(f) as in_f:
                c = json.load(in_f)
                if select_func(c):
                    i = i + 1
                    yield c

                if n_collections and i == n_collections:
                    L.info("Breaking early after %d collections.",
                           n_collections)
                    break

            if i % 1000 == 0:
                L.info("Yield collections, on iteration: %d.", i)

    def get_collections(self, select_func=lambda x: True):
        """Get Collections.

        Collect all the connections from the file.

        """

        return list(self.yield_collections(select_func))

    def yield_songs(self,
                    n_collections=None,
                    song_select_func=lambda x: True,
                    col_select_func=lambda x: True):
        """Get Songs.
        """

        for c in self.yield_collections(n_collections,
                                        select_func=col_select_func):
            for s in t.collection_to_songs(c, song_select_func):
                yield s

    def save_model(self, model, path):
        model.save(path)

    def load_model(self, path):
        return models.load_model(path)

class GcsRepository(Repository):
    """A Gcs Repository."""

    def __init__(self, bucket_name):
        self.bucket_name = bucket_name
        self._client = None

    @property
    def client(self):
        if self._client is None:
            self._client = storage.Client()
            return self._client
        else:
            return self._client

    def _yield_blobs(self, **kwargs):
        for blob in self.bucket.list_blobs(**kwargs):
            yield blob

    def yield_collections(self, select_func=lambda x: True, **kwargs):
        """Get collections.

        Args:
            select_func: A function that returns true if the
                function should be selected.
            kwargs: A list of keyword args to be passed to the blob function on gcs.

        """

        for b in self._yield_blobs(**kwargs):
            s = b.download_as_string().decode("utf-8")
            c = json.loads(s)
            if select_func(c):
                yield c

    @property
    def bucket(self):
        return self.client.get_bucket(self.bucket_name)

    def _get_most_recent_model_key(self):
        model_blobs = self.bucket.list_blobs(prefix="models/")
        most_recent = sorted(model_blobs, key=lambda x: x.name)
        return most_recent[-1].name

    def _get_model_tag(self, model_name):
        return model_name.split("/")[-1]

    def load_model(self, path=None):

        if path is None:
            path = self._get_most_recent_model_key()

        L.info("using path %s", path)
        model_tag = self._get_model_tag(path)
        blob = self.bucket.get_blob(path)

        if blob is None:
            raise Exception("key does not exist %s" % key)

        tmp_file = uuid.uuid4().hex
        with open(tmp_file, 'wb') as f:
            blob.download_to_file(f)

        model = models.load_model(tmp_file)

        return model, model_tag

    def save_model(self, model, path):
        tmp_file = uuid.uuid4().hex
        model.save(tmp_file)

        blob = bucket.blob(key)
        blob.upload_from_filename(filename=tmp_file)

