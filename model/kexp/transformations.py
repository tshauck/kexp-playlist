"""
Created By: Trent Hauck
Date: 2016-09-04T13:34:22
License: Private use of Trent Hauck
"""

# pylint: disable=no-member

from kexp.log import L

import numpy as np
import pandas as pd

STEP_SIZE = 2

def create_token_series(df, group_col="ShowAirDate", token_col="Song"):
    """ Create Token Serires.

    Given an input dataframe with a group_col and a token_col, group by
    the group_col and concatenate the items of the token_col together.  The
    resulting series has the group_col as its index.

    Args:
        df: The dataframe to be grouped.
        group_col: The column to group and index on.
        token_col: The column to concatenate into a single row.

    Returns:
        A pandas series.

    """

    return df.groupby(group_col)[token_col].apply(np.array)


def token_series_to_idx(show_lists, song_idx):

    def get_song_idx(song_list):
        """ Get Song Idx.
        """

        idx_list = np.ones_like(song_list)

        for i, s in enumerate(song_list):
            idx_list[i] = song_idx.get(s, 0)

        return idx_list

    return show_lists.apply(get_song_idx)


def create_indexes(df, token_col="Song"):
    """Create Indexes.

    Create the index by grouping the by token_col and counting.
    The index is then created by assigning sorting the resultant
    count in descending order.

    Args:
        token_col: Which column to group by.

    Return:
        Two series, (song -> idx, idx -> song).

    """

    grouped = df.groupby(token_col)[token_col].count().sort_values(
        ascending=False).to_frame()

    grouped['idx'] = np.arange(1, len(grouped) + 1)

    song_idx = grouped['idx']
    song_idx['OOV'] = 0

    idx_song = pd.Series(song_idx.index, index=song_idx)

    return song_idx.sort_values(), idx_song.sort_index()


def clip_X(X, max_n, min_len=-1, unknown=0, drop=False):
    """ Clip X.

    This assumes the tokens have already been replaced
    by their associated count from the index.

    Args:
        X: A data matrix with rows as collections
            and tokens have been replaced with their
            indexed amount.
        max_n: Keep the max_n number of words and replace
            those below with the oov char.
        min_len: Only keep sequences greater than or longer
            than the min_len.
        unknown: The index to use as the oov character.  Defaults
            to 1, which also is the most common value.
        drop: If drop is True then words > max_n are removed from
            the array. NB: If this is the case the actual sequence is
            not preserved.

    Returns:
        A copy of X with the max_n words replaced by the unknown character.
    """

    new_X = []

    for row in X:
        new_row = []

        for token in row:
            if token <= max_n:
                new_row.append(token)
            elif not drop:
                new_row.append(unknown)

        new_X.append(new_row)

    s = pd.Series(new_X, index=X.index)

    return s[s.apply(len) > min_len]


def collection_to_songs(collection, select_func=lambda x: True):
    """Collections to Songs.

    A json collections is passed and a flattened is yielded.

    Args:
        collection: A collection of songs.
        select_func: A function which selects the songs to be yielded.

    Yields:
        A json blob of flattened songs.

    """

    songs = collection.get("Songs", [])

    if not songs:
        return None

    for song in songs:
        if (not select_func(song)) or (song["ArtistName"] == "" or
                                       song["TrackName"] == ""):
            continue

        song["AirDateTime"] = pd.to_datetime(song["AirDateTime"])
        song["Day"] = collection["Day"]
        song["Hour"] = collection["Hour"]
        song["Month"] = collection["Month"]
        song["Period"] = collection["Period"]
        song["ShowAirDate"] = pd.to_datetime(collection["ShowAirDate"])
        song["ShowName"] = collection["ShowName"]
        song["Song"] = "{}::{}".format(song["ArtistName"], song["TrackName"])
        song["Year"] = collection["Year"]

        if "DefaultComments" in song:
            del song["DefaultComments"]

        yield song


def convert_show_sets_to_X_y(show_sets, seq_len=5, song_idx=None):
    """Convert Show Sets To X y.

    Convert a list of lists (shows by songs) into an X and y dataset. X is a
    matrix which has a width equal to `seq_len` and each entry is the index value
    of the song at that position. y is a vector with entries corresponding to the
    "next_song" in the sequence. What we're attempting to predict.

    """

    play_lists = []
    next_song = []

    for ss in show_sets:
        L.debug("Running for show_set (len=%d): %s.", len(ss), ss)
        for i in range(0, len(ss) - seq_len, STEP_SIZE):
            new_list = ss[i:i + seq_len]
            new_song = ss[i + seq_len]

            if song_idx is None:
                play_lists.append(new_list)
                next_song.append(ss[i + seq_len])
            else:
                play_lists.append([song_idx.get(x, 0) for x in new_list])
                next_song.append(song_idx.get(new_song, 0))

    X, y = np.array(play_lists), np.array(next_song)
    L.info("returning from convert_show_sets_to_X_y X_shape %s y_shape %s", X.shape, y.shape)
    return X, y


def encode_y(series, max_value):
    """Encode Input.

    Args:
        series: The series to sparsify.
        max_value: How many tokens are retained, determines the
            dimension of of the sparse dimension.
    """

    sparse = np.zeros((len(series), max_value), dtype=np.bool)
    for i, s in enumerate(series):
        sparse[i, s] = 1
    return sparse

def encode_X(X, max_value):
    """Encode Input.

    Args:
        series: The series to sparsify.
        max_value: How many tokens are retained, determines the
            dimension of of the sparse dimension.
        seq_len: How long the sequence should be.
    """

    len_X, width_X = X.shape
    sparse = np.zeros((len_X, width_X, max_value), dtype=np.bool)

    for row_n, row in enumerate(X):
        for col_n, col in enumerate(row):
            sparse[row_n, col_n, col] = 1

    return sparse

