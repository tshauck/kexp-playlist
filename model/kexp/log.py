import logging
from pythonjsonlogger import jsonlogger

L = logging.getLogger('model')
# L.setLevel(logging.INFO)

logHandler = logging.StreamHandler()
formatter = "%(asctime)s %(lineno)s %(message)s %(levelname)s %(module)s"

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def process_log_record(self, log_record):
        log_record["severity"] = log_record["levelname"].lower()
        del log_record["levelname"]
        return super(CustomJsonFormatter, self).process_log_record(log_record)

logHandler.setFormatter(CustomJsonFormatter(formatter))
L.addHandler(logHandler)
