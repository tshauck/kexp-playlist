"""
Created By: Trent Hauck
Date: 2016-08-19T15:09:11
License: Private use of Trent Hauck
"""

import pandas as pd
from sqlalchemy import create_engine, text
import numpy as np

import os

from kexp.log import L
import kexp.transformations as t

DEFAULT_SEQ_LEN = 5

CREATE_INDEX_TABLE = """
CREATE TABLE index_table (song varchar(200), cnt integer, idx integer, tag integer)
"""

CREATE_INDEX_QUERY = """
WITH base AS (
  SELECT artist_name || '|' || track_name song
  FROM kexp_playlist
  WHERE air_date >= '2009-01-01'
    AND NOT (track_name = '' OR release_name = '')
    AND NOT track_name = 'Weeks News in Review'
    AND NOT show_name = 'Variety Mix'
    AND NOT artist_name IN ('KEXP', 'Various')
),

counts AS (
  SELECT song, COUNT(*) cnt
  FROM base
  GROUP BY song
  ORDER BY cnt DESC
  LIMIT 20000
),

combo AS (
  SELECT 'UNK' song, -1 cnt, 0 idx

  UNION ALL

  SELECT song, cnt, ROW_NUMBER() OVER () idx
  FROM counts
)


INSERT INTO index_table

SELECT *, '{tag}' tag

FROM combo
ORDER BY idx
"""

GET_INDEX_QUERY = """
SELECT *
FROM index_table
WHERE tag = '{tag}'
"""


def create_sql_engine():
    """Create Engine.

    Create the sqlalchemy engine from the environment variables.
    """

    pg_user = os.getenv("POSTGRES_USER", "user")
    pg_pass = os.getenv("POSTGRES_PASSWORD", "pass")
    pg_db = os.getenv("POSTGRES_DB", "db")

    conn_str = "postgres://{pg_user}:{pg_pass}@kexpdb:5432/{pg_db}"
    conn_str = conn_str.format(pg_user=pg_user, pg_pass=pg_pass, pg_db=pg_db)

    return create_engine(conn_str)


def insert_tag_index(engine, tag):
    """Insert Tag Index.

    Create the song index to use for encoding.

    Args:
        engine: The sql alchemy engine to use
            for connection.
        tag: The tag which organizes a run.

    Return:
        (ResultProxy): The result of the query.
    """

    qry = CREATE_INDEX_QUERY.format(tag=tag)
    L.debug("creating index table with query \n%s", qry)

    try:
        engine.execute(text(qry).execution_options(autocommit=True))
        L.info("ran insert_tag_index query ok")
    except Exception as e:
        L.exception(e)
        raise e

def get_indexes(engine, tag):
    """Get Indexes

    Get the id to song and song id indexes.

    Args:
        engine: The sql alchemy engine to use
            for connection.
        tag: The tag which organizes a run.

    Returns:
        (dict, dict): A tuple of songs_to_index and index_to_songs

    """

    qry = GET_INDEX_QUERY.format(tag=tag)
    L.debug("getting index with query %s", qry)

    songs_to_index = {}
    index_to_songs = {}

    df = pd.read_sql_query(qry, engine)
    L.debug("read query to create the index dataframe with %d rows", len(df))
    for _, row in df.iterrows():
        songs_to_index[row.song] = row.idx
        index_to_songs[row.idx] = row.song

    return songs_to_index, index_to_songs


def get_playlist_dataset(engine):
    """Get Data.

    Get the song playlist data.

    Args:
        engine: The sqlalchemy engine.

    Returns:
        (pd.DataFrame): The dataframe with the song playlist data.
    """

    df = pd.read_sql("""
        SELECT *
        FROM kexp_playlist
        WHERE air_date >= '2011-01-01'
          AND NOT (track_name = '' OR release_name = '')
          AND NOT track_name = 'Weeks News in Review'
          AND NOT show_name = 'Variety Mix'
          AND NOT artist_name IN ('KEXP', 'Various')
    """, engine)

    L.info("got playlist dataset it has %d rows.", len(df))
    return df

def create_training_data(engine, tag):
    """Create Training Data.

    Create the training dataset.

    Args:
        engine: The sql alchemy engine to use
            for connection.
        tag: The tag which organizes a run.

    """

    insert_tag_index(engine, tag)
    songs_to_index, index_to_songs = get_indexes(engine, tag)

    L.info("created songs_to_index and index_to_songs len %d", len(index_to_songs))

    df = get_playlist_dataset(engine)

    show_sets = []

    df['artist_song'] = df.apply(
        lambda x: "{}|{}".format(x.artist_name, x.track_name),
        axis=1)

    for ((show, show_date), sdf) in df.groupby(['show_name', 'show_air_date']):
        L.debug("For show %s and showdate %s", show, show_date)

        songs = []
        for artist_song in sdf.artist_song:
            if artist_song in songs_to_index:
                songs.append(songs_to_index[artist_song])

        show_sets.append(songs)

    show_sets = np.array(show_sets)
    L.debug("The shape of show_sets is %s.", show_sets.shape)

    X, y = t.convert_show_sets_to_X_y(show_sets, seq_len=20)
    _, encoded_y = encode(X, y, len(songs_to_index))
    return X, encoded_y


def encode(dense_X, dense_y, n_features):
    """Hot Encode.

    Given a dense X and dense y encode replace the rows with a one hot vector that is one for
    whatever the index was.

    Args:
        dense_X: The input seq with a dense encoding.
        dense_y: The next song seq with a dense encoding.
        n_features: The number of features (unique songs)

    Return:
        sparse_X: The sparse representation of the input matrix.
        sparse_y: The sparse representation of the next song matrix.

    """

    n, _ = dense_X.shape

    sparse_y = np.zeros((n, n_features), dtype=np.bool)

    for i, _ in enumerate(dense_X):
        sparse_y[i, dense_y[i]] = 1

    return _, sparse_y


def create_index_table(engine, swallow_error=True):
    """Create Index Table.

    Args:
        engine: The sql alchemy engine to use
            for connection.
        swallow_error: If True, don't error log an error.
    """

    try:
        engine.execute(CREATE_INDEX_TABLE)
    except Exception as e:
        if swallow_error:
            return
        L.exception(e)
        raise e

