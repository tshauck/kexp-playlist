"""
Created By: Trent Hauck
Date: 2016-08-20T16:26:49
License: Private use of Trent Hauck
"""

import json
import falcon
import numpy as np

def _temperature(C):
    pass

def sample_p_vector(ps):
    return np.random.choice(np.arange(len(ps)), p=ps)

class Model(object):
    """ Model. """

    ROUTE = "/model"

    def __init__(self, model, logger, engine, songs_to_index, index_to_songs):
        self.model = model
        self.logger = logger
        self.engine = engine

        self.songs_to_index = songs_to_index
        self.index_to_songs = index_to_songs

    def on_post(self, req, resp):

        raw_json = req.stream.read()
        result_json = json.loads(raw_json.decode("utf-8"))

        set_length = result_json.get("set_length", 20)
        temperature = result_json.get("temperature", 1)

        songs = [self.songs_to_index[s]
                for s in result_json.get("songs", [])
                if s in self.songs_to_index]

        n_songs = len(songs)
        if len(songs) < set_length:
            songs = [0]*(set_length-n_songs) + songs

        new_songs = []

        for i in range(set_length):
            pred_array = np.array(songs).reshape(-1, set_length)
            predictions = self.model.predict(pred_array)[0]
            next_song = sample_p_vector(predictions)

            new_songs.append(next_song)
            songs = songs[1:] + [next_song]

        response_json = {"song_list": [self.index_to_songs[i] for i in songs],
                         "set_length": set_length,
                         "temperature": temperature}

        resp.body = json.dumps(response_json)
        resp.status = falcon.HTTP_200

