from datetime import datetime
import logging
import time

import click

import kexp.gcs as g
import kexp.model as m
import kexp.repository as r
import kexp.data as d
from kexp.log import L

L.setLevel(logging.INFO)

@click.group()
def cli():
    """cli.

    The command line interface.
    """

@cli.command()
def retrain_model():

    tag = datetime.now().strftime("%s")

    L.info("retraining model with tag %s", tag)
    bucket = g.get_bucket()

    engine = d.create_sql_engine()
    X, y = d.create_training_data(engine, tag)

    nrows, input_length = X.shape

    max_features = y.shape[1]
    input_length = 20

    model = m.build_model(max_features, input_length)

    L.info("Starting to fit model, X_dims:%s y_dims:%s", X.shape, y.shape)
    model.fit(X, y, batch_size=128, nb_epoch=20, verbose=2)

    L.info("done fitting model, uploading")
    g.save_model_to_gcs(model, bucket, tag)

