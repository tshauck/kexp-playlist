import kexp.transformations as t
import kexp.repository as r

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Activation, Dropout, Embedding, TimeDistributed
from keras.optimizers import RMSprop


class Model(object):

    def __init__(self, **kwargs):
        self.name = kwargs["name"]


class LSTMModel(Model):
    def __init__(self, **kwargs):

        super(LSTMModel, self).__init__(**kwargs)

        self.max_features = kwargs.get("max_features", 30000)
        self.seq_len = kwargs.get("seq_len", 10)
        self.drop = kwargs.get("drop", False)

        self.song_idx = kwargs["song_idx"]
        self.idx_song = kwargs["idx_song"]

        self._model = None


class LSTMBase(LSTMModel):
    def __init__(self, **kwargs):

        super(LSTMBase, self).__init__(**kwargs)
        self.lstm_dims = 128

    @property
    def model(self):
        if self._model is None:
            model = Sequential()
            model.add(LSTM(self.lstm_dims,
                           input_shape=(self.seq_len, self.max_features),
                           dropout_W=0.2,
                           dropout_U=0.2))
            model.add(Dense(self.max_features))
            model.add(Activation('softmax'))

            optimizer = RMSprop()
            model.compile(loss='categorical_crossentropy', optimizer=optimizer)

            self._model = model

            return self._model

        return self._model

    def fit(self, X, y):
        """ Assumes y is 2d (n_seq, max_features) """

        self.model.fit(X, y, batch_size=128, nb_epoch=3, verbose=2)
        return self

    def prepare(self, data):
        series = t.create_token_series(data)

        clipped_series = t.clip_X(idx_series,
                                  self.max_features,
                                  min_len=self.seq_len + 1,
                                  drop=self.drop)

        X, y = t.convert_show_sets_to_X_y(clipped_series, self.seq_len)

        encoded_y = t.encode_y(y, self.max_features)
        encoded_X = t.encode_X(X, self.max_features)
        return encoded_X, encoded_y


class LSTMEmbedded(LSTMModel):
    def __init__(self, **kwargs):

        super(LSTMEmbedded, self).__init__(**kwargs)
        self.embedding_dim = 128

    @property
    def model(self):
        if self._model is None:
            model = Sequential()
            model.add(Embedding(self.max_features,
                                self.embedding_dim,
                                input_length=self.seq_len,
                                dropout=0.2))

            model.add(LSTM(self.embedding_dim, dropout_W=0.2, dropout_U=0.2))
            model.add(Dense(self.max_features))
            model.add(Activation('softmax'))

            optimizer = RMSprop()
            model.compile(loss='categorical_crossentropy', optimizer=optimizer)

            self._model = model

            return self._model

        return self._model

    def fit(self, X, y, **kwargs):
        """ Assumes y is 2d (n_seq, max_features) """

        nb_epoch = kwargs.pop("nb_epoch", 20)

        self.model.fit(X, y, nb_epoch=nb_epoch, verbose=2, **kwargs)
        return self

    def prepare(self, data):
        series = t.create_token_series(data)

        idx_series = series.apply(self._get_song_idx)

        clipped_series = t.clip_X(idx_series,
                                  self.max_features,
                                  min_len=self.seq_len + 1,
                                  drop=self.drop)

        X, y = t.convert_show_sets_to_X_y(clipped_series, self.seq_len)

        encoded_y = t.encode_y(y, self.max_features)
        return X, encoded_y

class LSTMEmbeddedDropout(LSTMEmbedded):
    def __init__(self, **kwargs):
        super(LSTMEmbeddedDropout, self).__init__(**kwargs)
        self.embedding_dim = 128

    @property
    def model(self):
        if self._model is None:
            model = Sequential()
            model.add(Embedding(self.max_features,
                                self.embedding_dim,
                                input_length=self.seq_len,
                                dropout=0.2))

            model.add(LSTM(self.embedding_dim, dropout_W=0.2, dropout_U=0.2, return_sequences=True))

            model.add(TimeDistributed(Dense(8)))

            model.add(Dropout(.5))
            model.add(LSTM(8))
            model.add(Dropout(.5))

            model.add(Dense(self.max_features))
            model.add(Activation('softmax'))

            optimizer = RMSprop()
            model.compile(loss='categorical_crossentropy', optimizer=optimizer)

            self._model = model

            return self._model

        return self._model

