"""
Created By: Trent Hauck
Date: 2016-08-19T13:57:45
License: Private use of Trent Hauck
"""

from gcloud import storage
from keras import models

import logging
import uuid

BUCKET = "tshauck-kexp-project"
MODEL_TEMPLATE = "model-{tag}.h5"


def get_bucket():
    """Return the Bucket object which is used to save and
    download files from gcs.

    """

    client = storage.Client()
    return client.get_bucket(BUCKET)

def save_model_to_gcs(model, bucket, key):
    """Save Model To Gcs.

    Args:
        model: The keras model to save to gcs.
        bucket: The GCS bucket object.
        key: The key where to save the model.

    """

    tmp_file = uuid.uuid4().hex
    model.save(tmp_file)

    blob = bucket.blob("models/%s" % key)
    blob.upload_from_filename(filename=tmp_file)


def load_model_from_gcs(bucket, key):
    """Load Model To Gcs.

    Args:
        bucket: The GCS bucket object.
        key: The key where the model is saved.

    Returns:
        model: The keras model to be loaded.

    """

    tmp_file = uuid.uuid4().hex

    blob = bucket.get_blob(key)

    if blob is None:
        raise Exception("key does not exist %s", key)

    with open(tmp_file, 'wb') as f:
        blob.download_to_file(f)

    model = models.load_model(tmp_file)

    return model
