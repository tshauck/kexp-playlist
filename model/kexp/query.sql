WITH base AS (
  SELECT track_name || '|' || release_name song
  FROM kexp_playlist
),

counts AS (
  SELECT song, COUNT(*) cnt
  FROM base
  GROUP BY song
  LIMIT 20000
),

combo AS (
  SELECT song, cnt
  FROM counts

  UNION ALL

  SELECT 'UNK' song, 1 cnt
)


INSERT INTO index_table 

SELECT *,
       ROW_NUMBER() OVER () id,
       'test'::varchar(20) tag

FROM combo

SELECT *
FROM index_table
WHERE tag = 'test'
