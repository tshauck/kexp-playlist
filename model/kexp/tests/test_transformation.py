"""
Created By: Trent Hauck
Date: 2016-09-04T18:05:31
License: Private use of Trent Hauck
"""

# pylint: disable=no-member

import unittest
import logging
from copy import copy

import numpy as np
import pandas as pd

from kexp import transformations as t
from kexp.log import L

L.setLevel(logging.DEBUG)

TEST_COLLECTIONS = [
    {
        "Year": 2016,
        "Month": 5,
        "Day": 4,
        "Hour": 2,
        "Djs": [
            "Kevin Cole"
        ],
        "ShowName": "The Afternoon Show",
        "ShowAirDate": "Wednesday, May 4, 2016 - 2 PM",
        "Period": "PM",
        "Songs": [
            {
                "AirDate": "/Date(1462399087000)/",
                "AirDateTime": "2016-05-04T14:58:07-07:00",
                "ArtistName": "The Smiths",
                "DefaultComments": [],
                "LabelName": "Reprise",
                "IsLocal": "false",
                "LargeExtImage":
                "http://ecx.images-amazon.com/images/I/41C3MPXAKVL.jpg",
                "PlayUri": "http://services.kexp.org/rest/plays/2192993",
                "TrackName": "There Is A Light That Never Goes Out",
                "ReleaseName": "\"Singles\""
            },
        ]
    }, {
        "Year": 2016,
        "Month": 5,
        "Day": 4,
        "Hour": 1,
        "Djs": [
            "Evie"
        ],
        "ShowName": "Variety Mix",
        "ShowAirDate": "Wednesday, May 4, 2016 - 1 AM",
        "Period": "AM",
        "Songs": [
            {
                "AirDate": "/Date(1462352149000)/",
                "AirDateTime": "2016-05-04T01:55:49-07:00",
                "ArtistName": "Wild Nothing",
                "DefaultComments": [],
                "LabelName": "Captured Tracks",
                "IsLocal": "false",
                "LargeExtImage":
                "http://ecx.images-amazon.com/images/I/61lzIwNJnoL.jpg",
                "PlayUri": "http://services.kexp.org/rest/plays/2192789",
                "TrackName": "To Know You",
                "ReleaseName": "Life of Pause"
            }, {
                "AirDate": "/Date(1462351849000)/",
                "AirDateTime": "2016-05-04T01:50:49-07:00",
                "ArtistName": "Grimes",
                "DefaultComments": [],
                "LabelName": "4AD",
                "IsLocal": "true",
                "LargeExtImage":
                "http://ecx.images-amazon.com/images/I/61WdgVbTHpL.jpg",
                "PlayUri": "http://services.kexp.org/rest/plays/2192788",
                "TrackName": "REALiTi",
                "ReleaseName": "Art Angels"
            }
        ]
    }
]

SONGS = []
for col in TEST_COLLECTIONS:
    for song in t.collection_to_songs(col):
        SONGS.append(song)

class TestTransformations(unittest.TestCase):
    """Test the GCS Config.
    """

    def test_clip_X(self):

        test_X = np.arange(1, 10).reshape(3, 3)
        clipped_X = t.clip_X(test_X, 2)

        expected_array = np.array([[1, 2, 1], [1, 1, 1], [1, 1, 1]])

        self.assertTrue(np.array_equal(clipped_X, expected_array))

    def test_clip_X_drop(self):

        test_X = np.arange(1, 10).reshape(3, 3)
        clipped_X = t.clip_X(test_X, 2, drop=True)

        expected_array = np.array([[1, 2], [], []])

        self.assertTrue(np.array_equal(clipped_X, expected_array))

    def test_collection_to_songs(self):

        expected_song = {
            "AirDateTime": pd.to_datetime("2016-05-04T14:58:07-07:00"),
            "ArtistName": "The Smiths",
            "Day": 4,
            "Hour": 2,
            "IsLocal": "false",
            "LabelName": "Reprise",
            "LargeExtImage":
            "http://ecx.images-amazon.com/images/I/41C3MPXAKVL.jpg",
            "Month": 5,
            "Period": "PM",
            "PlayUri": "http://services.kexp.org/rest/plays/2192993",
            "ReleaseName": "\"Singles\"",
            "ShowAirDate": pd.to_datetime("Wednesday, May 4, 2016 - 2 PM"),
            "ShowName": "The Afternoon Show",
            "TrackName": "There Is A Light That Never Goes Out",
            "DefaultComments": [],
            "AirDate": "/Date(1462399087000)/",
            "Year": 2016,
        }

        expected_song["Song"] = "{}::{}".format(expected_song["ArtistName"],
                                                expected_song["TrackName"])

        actual_song = list(t.collection_to_songs(TEST_COLLECTIONS[0]))[0]

        self.assertDictEqual(expected_song, actual_song)

    def test_collection_select_func(self):

        # test select_func
        actual_song = list(t.collection_to_songs(TEST_COLLECTIONS[
            1], lambda x: x["IsLocal"] == "false"))

        self.assertEqual(len(actual_song), 1)

        actual_song = list(t.collection_to_songs(TEST_COLLECTIONS[
            1], lambda x: x["IsLocal"] == "true"))

        self.assertEqual(len(actual_song), 1)

    def test_create_token_series(self):

        songs_df = pd.DataFrame(SONGS)

        series = t.create_token_series(songs_df, "ShowAirDate", "Song")
        self.assertEqual(len(series), 2)

    def test_create_indexes(self):

        songs_df = pd.DataFrame(SONGS)
        song_idx, idx_song = t.create_indexes(songs_df)

        self.assertEqual(len(song_idx), 3)
        self.assertEqual(song_idx.index.name, "Song")

        self.assertEqual(len(idx_song), 3)
        self.assertEqual(idx_song.index.name, "idx")
        self.assertEqual(idx_song.name, "Song")

