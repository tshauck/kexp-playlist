import unittest
from unittest.mock import Mock, patch

from kexp import gcs as g

class TestGcs(unittest.TestCase):
    """Test the GCS Config.
    """

    def test_save_model_to_gcs(self):
        model = Mock()
        bucket = Mock()
        blob = Mock()
        bucket.blob.return_value = blob
        key = "test_key"

        g.save_model_to_gcs(model, bucket, key)

        self.assertTrue(model.save.called)
        bucket.blob.assert_called_with(key)
        self.assertTrue(blob.upload_from_filename.called)

    @patch('keras.models.load_model')
    def test_load_model_from_gcs(self, load_model_mock):

        bucket = Mock()
        blob = Mock()
        bucket.get_blob.return_value = blob

        key = "test_key"

        g.load_model_from_gcs(bucket, key)

        self.assertTrue(blob.download_to_file.called)
        self.assertTrue(load_model_mock.called)

