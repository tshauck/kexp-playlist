#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    \connect "$POSTGRES_DB";

    CREATE TABLE kexp_playlist (
            id bigserial PRIMARY KEY,
            year int,
            month int,
            day int,
            hour int,
            show_name varchar(100),
            show_air_date varchar(100),
            artist_name varchar(100),
            air_date timestamp,
            label_name varchar(100),
            track_name varchar(100),
            release_name varchar(100),
            main_dj varchar(100),
            period varchar(20),
            CONSTRAINT one_song_per_play_time unique (air_date)
    );

    CREATE TABLE index_table (song varchar(200), cnt integer, idx integer, tag varchar(200));
EOSQL
