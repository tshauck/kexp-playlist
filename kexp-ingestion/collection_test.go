package main

import (
	"testing"
)

func TestLoadCollection(t *testing.T) {
	_, err := CollectionFromBytes(testCollection)
	if err != nil {
		t.Errorf("Got error loading collection: %v.", err)
	}
}
