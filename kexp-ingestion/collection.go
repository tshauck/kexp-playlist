package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"regexp"
	"strconv"
	"time"
)

const dataDir = "data"

var dtRegexp = regexp.MustCompile(`Date\((.*)\)`)
var Pacific = time.FixedZone("Pacific", -8*3600)

type Collection struct {
	Year        int `db:"year"`
	Month       int `db:"month"`
	Day         int `db:"day"`
	Hour        int `db:"hour"`
	Djs         []string
	ShowName    string
	ShowAirDate string
	Period      string `db:"period"`
	Songs       []Song
}

func CollectionFromBytes(b []byte) (*Collection, error) {
	var c *Collection
	err := json.Unmarshal(b, &c)
	if err != nil {
		return nil, err
	}
	return c, nil

}

func (c Collection) Flatten() []map[string]interface{} {

	var rows []map[string]interface{}

	for _, s := range c.Songs {
		row := map[string]interface{}{
			"Year":        c.Year,
			"Month":       c.Month,
			"Day":         c.Day,
			"Hour":        c.Hour,
			"ShowName":    c.ShowName,
			"ShowAirDate": c.ShowAirDate,
			"ArtistName":  s.ArtistName,
			"AirDate":     s.AirDateTime,
			"LabelName":   s.LabelName,
			"TrackName":   s.TrackName,
			"ReleaseName": s.ReleaseName,
			"Period":      c.Period,
		}

		if len(c.Djs) > 0 {
			row["MainDj"] = c.Djs[0]
		} else {
			row["MainDj"] = ""
		}
		rows = append(rows, row)
	}

	return rows
}

func (c Collection) FlattenedJsonBytes() ([]byte, error) {
	var bs [][]byte

	flattened := c.Flatten()

	for _, f := range flattened {
		b, err := json.Marshal(f)
		if err != nil {
			return nil, err
		}
		bs = append(bs, b)
	}

	return bytes.Join(bs, []byte("\n")), nil
}

func (c Collection) JsonBytes() ([]byte, error) {
	return json.Marshal(c)
}

func (c Collection) Path() string {
	return fmt.Sprintf("%d/%d/%d/%d%s", c.Year, c.Month, c.Day, c.Hour, c.Period)
}

func (c Collection) PaddedPath() string {
	return fmt.Sprintf("%d/%02d/%02d/%02d%s", c.Year, c.Month, c.Day, c.Hour, c.Period)
}

func (c Collection) Uri() string {
	return fmt.Sprintf("http://kexp.org/playlist/%s", c.Path())
}

func (c *Collection) AddShow(doc *goquery.Document) {
	doc.Find(".ShowName > a").Each(func(i int, s *goquery.Selection) {
		c.ShowName = s.Text()
	})

	doc.Find(".ShowAirDate").Each(func(i int, s *goquery.Selection) {
		c.ShowAirDate = s.Text()
	})

	var djs []string
	doc.Find(".ShowHost > a").Each(func(i int, s *goquery.Selection) {
		djs = append(djs, s.Text())
	})
	c.Djs = djs
}

func (c *Collection) AddSongs(doc *goquery.Document) {

	var newSongs []Song
	doc.Find(".Play").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		playlistData, ok := s.Attr("data-playlistitem")
		if !ok {
			fmt.Println(ok)
		}

		var song Song
		err := json.Unmarshal([]byte(playlistData), &song)
		if err != nil {
			panic(err)
		}

		err = song.AddAirDateTime()
		if err != nil {
			panic(err)
		}
		newSongs = append(newSongs, song)
	})

	c.Songs = newSongs
}

type Song struct {
	AirDate         string
	AirDateTime     time.Time
	ArtistName      string
	DefaultComments []DefaultComment
	LabelName       string
	IsLocal         bool
	LargeExtImage   string
	PlayUri         string
	TrackName       string
	ReleaseName     string
}

func (s *Song) AddAirDateTime() error {

	matches := dtRegexp.FindAllStringSubmatch(s.AirDate, -1)
	if len(matches) == 0 || len(matches[0]) == 0 {
		return fmt.Errorf("issue with matches %s", matches)
	}

	d, err := msToTime(matches[0][1])
	if err != nil {
		return err
	}

	s.AirDateTime = d
	return nil
}

func (s Song) String() string {
	return fmt.Sprintf("Song{ArtistName: %s, TrackName: %s, ReleaseName: %s}", s.ArtistName, s.TrackName, s.ReleaseName)
}

type ReleaseEventDate struct {
	Day   int
	Month int
	Year  int
}

type DefaultComment struct {
	CreatedDate string // should be a time.Time
	IsDeleted   bool
	Text        string
	UpdateDate  string
	Uri         string
	User        User
}

type User struct {
	DisplayName string
}

func msToTime(ms string) (time.Time, error) {
	msInt, err := strconv.ParseInt(ms, 10, 64)
	if err != nil {
		return time.Time{}, err
	}

	return time.Unix(0, msInt*int64(time.Millisecond)), nil
}

func (c *Collection) Process() error {
	doc, err := goquery.NewDocument(c.Uri())

	if err != nil {
		log.Fatal(err)
	}
	c.AddShow(doc)
	c.AddSongs(doc)

	return nil
}

func (c Collection) Date() time.Time {
	hour := convert12To24(c.Hour, c.Period)
	return time.Date(c.Year, time.Month(c.Month), c.Day, hour, 0, 0, 0, Pacific)
}

func CollectionsBetween(t1 time.Time, t2 time.Time) []Collection {

	var cs []Collection

	var hour int
	var period string

	for {
		if t1.After(t2) {
			break
		}

		t1 = t1.Add(1 * time.Hour)

		hour, period = convert24To12(t1.Hour())

		c := Collection{
			Year:   t1.Year(),
			Month:  int(t1.Month()),
			Day:    t1.Day(),
			Hour:   hour,
			Period: period,
		}

		cs = append(cs, c)

	}

	return cs
}

// convert the 12 hour input to output
func convert12To24(i int, p string) int {

	// Really 12AM is the 0 hour
	if i == 12 && p == "AM" {
		return 0
	}
	// If it isn't 12am, but we're in the AM
	// just return the hour
	if p == "AM" {
		return i
	}
	// If we're in the PM, but the hour is 12
	// leave it at 12.
	if i == 12 && p == "PM" {
		return i
	}
	if p == "PM" {
		return i + 12
	}
	return 0
}

// convert 24 hour to kexp 12 hour
func convert24To12(i int) (int, string) {
	if i == 0 {
		return 12, "AM"
	}

	if i == 12 {
		return 12, "PM"
	}

	if i < 12 {
		return i, "AM"
	}

	if i > 12 {
		return i - 12, "PM"
	}

	return 0, ""
}
