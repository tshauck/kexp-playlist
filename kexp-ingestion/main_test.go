package main

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	code := m.Run()
	os.Exit(code)
}

var testCollection = []byte(`
{
  "Year":2016,
  "Month":1,
  "Day":1,
  "Hour":1,
  "Djs":["DJ Shannon"],
  "ShowName":"The Midday Show",
  "ShowAirDate":"Friday, January 1, 2016 - 10 AM",
  "Period":"PM",
  "Songs": [
  	{
	  "AirDate":"/Date(1451685360000)/",
	  "AirDateTime":"2016-01-01T13:56:00-08:00",
	  "ArtistName":"Tindersticks",
	  "DefaultComments":[],
	  "LabelName":"City Slang",
	  "IsLocal":false,
	  "LargeExtImage":"http://ecx.images-amazon.com/images/I/513s5FWVhNL.jpg",
	  "PlayUri":"http://services.kexp.org/rest/plays/2138858",
	  "TrackName":"We Are Dreamers!",
	  "ReleaseName":"The Waiting Room"
      }
   ]
}
`)
