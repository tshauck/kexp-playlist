package main

import (
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/levels"
	"github.com/urfave/cli"
	"os"
	"time"
)

const (
	WriteModeBoth    = "both"
	WriteModeDB      = "db"
	WriteModeStorage = "storage"
)

type Service struct {
	L  levels.Levels
	Sd *SongDatabase
	Ss *SongStorage
}

func NewService(writeMode string) Service {
	l := levels.New(log.NewJSONLogger(os.Stdout), levels.Key("severity"))

	switch writeMode {
	case WriteModeBoth:
		sd := MustCreateSongDatabase()
		ss := MustCreateSongStorage()
		return Service{L: l, Sd: sd, Ss: ss}
	case WriteModeDB:
		sd := MustCreateSongDatabase()
		return Service{L: l, Sd: sd, Ss: nil}
	default:
		ss := MustCreateSongStorage()
		return Service{L: l, Sd: nil, Ss: ss}
	}
}

func (s Service) BackFillToCollection(c Collection) error {
	start, _ := time.Parse("2006-01-02", "2010-09-01")
	cDate := c.Date()

	s.L.Debug().Log("msg", "getting collections between dates", "start",
		start, "end", cDate)

	cs := CollectionsBetween(start, cDate)
	return s.ProcessCollections(cs)
}

func (s Service) BackFillFromCollection(c Collection) error {
	now := time.Now()
	cDate := c.Date()
	s.L.Debug().Log("msg", "getting collections between dates", "start",
		cDate, "end", now)
	cs := CollectionsBetween(cDate, now)
	return s.ProcessCollections(cs)
}

func (s Service) ProcessCollections(cs []Collection) error {
	var err error

	for _, c := range cs {
		s.L.Debug().Log("msg", "processing collection",
			"collection_date", c.Date(), "year", c.Year, "month", c.Month,
			"hour", c.Hour, "period", c.Period, "uri", c.Uri())

		err = c.Process()
		if err != nil {
			s.L.Error().Log("msg", "error processing collection",
				"err", err,
				"collection_date", c.Date())
			continue
		}
		s.L.Debug().Log("msg", "done processing collection",
			"collection_date", c.Date(), "n_songs", len(c.Songs))

		if len(c.Songs) == 0 {
			continue
		}

		err = s.Sd.SaveCollection(c)
		if err != nil {
			s.L.Error().Log("msg", "error saving collection to db",
				"err", err,
				"collection_date", c.Date())
		}

		err = s.Ss.SaveCollection(c)
		if err != nil {
			s.L.Error().Log("msg", "error saving collection to gcs",
				"err", err,
				"collection_date", c.Date())
		}
	}

	return err
}

func main() {
	Run()
}

func Run() {
	app := cli.NewApp()
	app.Name = "kexp-exp"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "bucket",
			Usage:  "The GCS Bucket to use for storage.",
			EnvVar: "BUCKET",
			Value:  "tshauck-kexp-project",
		},
	}

	getSingleCollectionFlags := []cli.Flag{
		cli.IntFlag{Name: "year"},
		cli.IntFlag{Name: "month"},
		cli.IntFlag{Name: "day"},
		cli.IntFlag{Name: "hour"},
		cli.StringFlag{Name: "period"},
		cli.StringFlag{Name: "write-mode", Value: "both"},
	}

	getSingleCollection := cli.Command{
		Name:   "single-collection",
		Usage:  "Get a single collection of songs.",
		Action: CmdGetSingleCollection,
		Flags:  getSingleCollectionFlags,
	}

	fillBetweenFlags := []cli.Flag{
		cli.StringFlag{Name: "start"},
		cli.StringFlag{Name: "end"},
	}

	fillBetweenCommand := cli.Command{
		Name:   "fill-between",
		Usage:  "Fill between two dates.",
		Action: CmdFillBetween,
		Flags:  fillBetweenFlags,
	}

	autoIngestion := cli.Command{
		Name:   "auto-ingestion",
		Usage:  "Get all un-ingested collections.",
		Action: CmdAutoIngestion,
	}

	backFillTo := cli.Command{
		Name:   "backfill-to",
		Usage:  "Read from 1/1/2001 until the first collection ingested.",
		Action: CmdBackfillFromHistory,
	}

	app.Commands = []cli.Command{
		getSingleCollection,
		autoIngestion,
		backFillTo,
		fillBetweenCommand,
	}

	app.Run(os.Args)
}

func CmdAutoIngestion(c *cli.Context) error {
	s := NewService(WriteModeBoth)
	coll, err := s.Sd.LastCollectionDownloaded()
	if err != nil {
		return err
	}
	return s.BackFillFromCollection(*coll)
}

func CmdBackfillFromHistory(c *cli.Context) error {
	s := NewService(WriteModeBoth)
	coll, err := s.Sd.LastCollectionDownloaded()
	if err != nil {
		return err
	}
	return s.BackFillToCollection(*coll)
}

func CmdGetSingleCollection(c *cli.Context) error {

	writeMode := c.String("write-mode")
	s := NewService(writeMode)

	year := c.Int("year")
	month := c.Int("month")
	day := c.Int("day")
	hour := c.Int("hour")
	period := c.String("period")

	cn := Collection{
		Year:   year,
		Month:  month,
		Day:    day,
		Hour:   hour,
		Period: period,
	}

	err := cn.Process()
	if err != nil {
		return err
	}

	switch writeMode {
	case WriteModeBoth:
		s.L.Debug().Log("msg", "write mode is both")
		err = s.Sd.SaveCollection(cn)
		if err != nil {
			return err
		}
		return s.Ss.SaveCollection(cn)
	case WriteModeDB:
		s.L.Debug().Log("msg", "write mode is db")
		return s.Sd.SaveCollection(cn)
	case WriteModeStorage:
		err = s.Ss.SaveCollection(cn)
		s.L.Debug().Log("msg", "write mode is storage", "err", err)
		return err
	default:
		s.L.Debug().Log("msg", "write mode is unknown")
		return fmt.Errorf("unrecognized write mode %s", writeMode)
	}
}

func CmdFillBetween(c *cli.Context) error {
	s := NewService(WriteModeStorage)

	start, _ := time.Parse("2006-01-02", c.String("start"))
	end, _ := time.Parse("2006-01-02", c.String("end"))

	cs := CollectionsBetween(start, end)
	return s.ProcessCollections(cs)
}
