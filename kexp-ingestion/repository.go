package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"golang.org/x/net/context"
	"google.golang.org/cloud/storage"
	"os"
)

const kexpPlaylistTable = `
	CREATE TABLE kexp_playlist (
	  	id bigserial PRIMARY KEY,
	  	year int,
		month int,
		day int,
		hour int,
		show_name varchar(100),
		show_air_date varchar(100),
		artist_name varchar(100),
		air_date timestamp,
		label_name varchar(100),
		track_name varchar(100),
		release_name varchar(100),
		main_dj varchar(100),
		period varchar(20),
		CONSTRAINT one_song_per_play_time unique (air_date)
	);
`

const BUCKETNAME = "tshauck-kexp-project"

type SongDatabase struct {
	db *sqlx.DB
}

func NewSongDatabase() (*SongDatabase, error) {
	dbname := os.Getenv("POSTGRES_DB")
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")

	c := fmt.Sprintf("sslmode=disable host=kexpdb dbname=%s user=%s password=%s",
		dbname, user, pass)

	conn, err := sqlx.Connect("postgres", c)
	if err != nil {
		return nil, err
	}
	return &SongDatabase{db: conn}, nil
}

func MustCreateSongDatabase() *SongDatabase {
	db, err := NewSongDatabase()
	if err != nil {
		panic(err)
	}
	return db

}

func (sd SongDatabase) ConfigureDatabase() error {
	_, err := sd.db.Exec(kexpPlaylistTable)
	return err
}

func (sd SongDatabase) SaveCollection(c Collection) error {
	// Save the collection, but rollback on an error
	var err error
	var rerr error

	flattenedC := c.Flatten()

	for _, row := range flattenedC {
		_, err = sd.db.NamedExec(`INSERT INTO kexp_playlist (year, month,
			day, hour, show_name, show_air_date, artist_name, air_date,
			label_name, track_name, release_name, main_dj, period) VALUES (:Year,
			:Month, :Day, :Hour, :ShowName, :ShowAirDate, :ArtistName,
			:AirDate, :LabelName, :TrackName, :ReleaseName, :MainDj, :Period)`,
			row)

		if err != nil {
			rerr = fmt.Errorf("error inserting song %s error: %s", row["TrackName"], err)
		}
	}
	return rerr
}

func (sd SongDatabase) LastCollectionDownloaded() (*Collection, error) {
	c := Collection{}

	qry := `SELECT DISTINCT year, month, day, hour, period
		FROM kexp_playlist ORDER BY 1 DESC, 2 DESC, 3 DESC, 4 DESC, 5 DESC LIMIT 1`

	err := sd.db.Get(&c, qry)
	if err != nil {
		return nil, err
	}

	return &c, nil

}

func (sd SongDatabase) FirstCollectionDownloaded() (*Collection, error) {
	c := Collection{}

	qry := `SELECT DISTINCT year, month, day, hour, period
		FROM kexp_playlist ORDER BY 1, 2, 3, 4, 5 LIMIT 1`

	err := sd.db.Get(&c, qry)
	if err != nil {
		return nil, err
	}

	return &c, nil

}

type SongStorage struct {
	client *storage.Client
	bucket string
}

func (ss SongStorage) Close() error {
	return ss.client.Close()
}

func NewSongStorage() (*SongStorage, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bucket := os.Getenv("BUCKET")
	if bucket == "" {
		return nil, fmt.Errorf("Error getting bucket from $BUCKET")
	}
	return &SongStorage{client: client, bucket: bucket}, nil
}

func MustCreateSongStorage() *SongStorage {
	ss, err := NewSongStorage()
	if err != nil {
		panic(err)
	}
	return ss

}

func (ss SongStorage) SaveCollection(c Collection) error {

	ctx := context.Background()
	wc := ss.client.Bucket(ss.bucket).Object(c.PaddedPath()).NewWriter(ctx)

	wc.ContentType = "text/plain"
	wc.ACL = []storage.ACLRule{{storage.AllUsers, storage.RoleReader}}

	b, err := c.JsonBytes()
	if err != nil {
		return err
	}

	_, err = wc.Write(b)
	if err != nil {
		return err
	}

	return wc.Close()
}
